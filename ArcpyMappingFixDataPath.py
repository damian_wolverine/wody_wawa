import arcpy, os, sys, exceptions, traceback

a = r'e:\WODY_WAWA\OLA\ANALIZY_WERSJA_OST\W_ANALIZY_LAYOUT.mxd'
mxd = arcpy.mapping.MapDocument(a)

basePath = r'E:\WODY_WAWA\OLA'
splitPath = r'E:\Warszawa'
cnt = 0

# # loop for chcecking how layers print in console
# checkingList = []
# for layer in arcpy.mapping.ListLayers(mxd):
#     if layer.supports("DATASOURCE"):
#         print cnt, layer.isBroken
#         checkingList.append(layer.isBroken)
#         cnt += 1
# print "print"

listRepetLayer = []
for layer in arcpy.mapping.ListLayers(mxd):
    #print "dupa"
    try:
        if layer.supports("DATASOURCE") and layer.isBroken:
            lenOfSplitWorkspace = len(layer.workspacePath.split(splitPath))
            if (layer.workspacePath not in listRepetLayer) and (lenOfSplitWorkspace > 1):
                #print layer.dataSource, cnt
                #print (layer.dataSource)
                print layer.datasetName, layer.workspacePath.split(), layer.workspacePath.split(splitPath)
                newPath = basePath + layer.workspacePath.split(splitPath)[1]
                # splitName = layer.datasetName
                print (layer.workspacePath, newPath)
                mxd.findAndReplaceWorkspacePaths(layer.workspacePath, newPath)
                listRepetLayer.append(layer.workspacePath)
                print "po zmianie", listRepetLayer
            else:
                print "skrypt idzie dalej"
                pass

    except arcpy.ExecuteError:
        print arcpy.GetMessages()

    except:
        # Get the traceback object
        tb = sys.exc_info()[2]
        tbinfo = traceback.format_tb(tb)[0]
        # Concatenate error information into message string
        pymsg = 'PYTHON ERRORS:\nTraceback info:\n{0}\nError Info:\n{1}'.format(tbinfo, str(sys.exc_info()[1]))
        msgs = 'ArcPy ERRORS:\n {0}\n'.format(arcpy.GetMessages(2))
        # Return python error messages for script tool or Python Window
        arcpy.AddError(pymsg)
        arcpy.AddError(msgs)

mxd.saveACopy(r'e:\WODY_WAWA\OLA\ANALIZY_WERSJA_OST\W_ANALIZY_LAYOUT_dm.mxd')
del mxd
