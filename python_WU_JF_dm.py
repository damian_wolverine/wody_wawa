
# Skrypt do wyznaczania wartosci WU dla jednostek funkcjonalnych Warszawy

import arcpy 
arcpy.env.workspace = 'E:/Warszawa/ANALIZY_WERSJA_OST/ANALIZA_WU_JFUNKC/POPRAWKA_23072015/ANALIZA_WU_JF_POP.gdb'

# arcpy.AddField_management("jedn_funkc", "PowCB_SI", "DOUBLE")
# arcpy.AddField_management("jedn_funkc", "PowPBC_SI", "DOUBLE")
# arcpy.AddField_management("jedn_funkc", "PowPNP_SI", "DOUBLE")
# arcpy.AddField_management("jedn_funkc", "PBC_SI", "DOUBLE")
# arcpy.AddField_management("jedn_funkc", "IZB_SI", "DOUBLE")
# arcpy.AddField_management("jedn_funkc", "SLK_SI", "DOUBLE")
# arcpy.AddField_management("jedn_funkc", "PNP_SI", "DOUBLE")

# PBC
arcpy.Union_analysis(["PBC_zielen_id4", "wsk_urb_ter_uz"], "wsk_urb_PBC", "ALL")

arcpy.SelectLayerByAttribute_management("wsk_urb_PBC", "NEW_SELECTION", ''' "FID_PBC_zielen_id4" = -1 ''')
arcpy.DeleteRows_management("wsk_urb_PBC")
arcpy.SelectLayerByAttribute_management("wsk_urb_PBC","CLEAR_SELECTION")

arcpy.SelectLayerByAttribute_management("wsk_urb_PBC", "NEW_SELECTION", ''' "FID_wsk_urb_ter_uz" = -1 ''')
arcpy.DeleteRows_management("wsk_urb_PBC")
arcpy.SelectLayerByAttribute_management("wsk_urb_PBC","CLEAR_SELECTION")

arcpy.Statistics_analysis("wsk_urb_PBC", "Tabela_PBC", [["Shape_Area","SUM"]], ["OZN_JDN"])

arcpy.JoinField_management("wsk_urb_ter_uz", "OZN_JDN", "Tabela_PBC", "OZN_JDN")
arcpy.CalculateField_management("wsk_urb_ter_uz", "PowPBC_SI", '!SUM_Shape_Area!', "PYTHON")
arcpy.DeleteField_management("wsk_urb_ter_uz", ["OZN_JDN_1","SUM_Shape_Area","FREQUENCY"])

arcpy.CalculateField_management("wsk_urb_ter_uz", "PBC_SI", '(!PowPBC_SI! / !Shape_Area!) * 100', "PYTHON")

arcpy.SelectLayerByAttribute_management("wsk_urb_ter_uz", "NEW_SELECTION", '''"PBC_SI" is null ''')
result = int(arcpy.GetCount_management("wsk_urb_ter_uz")[0]) # zwraca liczbe od 0 do n, gdzie n = liczba rekordów wsk_urb_ter_uz
if result:
    print ("warunek spelniony, przeliczany pole PBC_SI is null")
    arcpy.CalculateField_management("wsk_urb_ter_uz", "PBC_SI", "0","PYTHON_9.3")
print ("without if")

# IZB i SLK
arcpy.Union_analysis(["budynki", "jedn_funkc"], "jedn_budynki", "ALL")

arcpy.SelectLayerByAttribute_management("jedn_budynki", "NEW_SELECTION", ''' "FID_budynki" = -1 ''')
arcpy.DeleteRows_management("jedn_budynki")
arcpy.SelectLayerByAttribute_management("jedn_budynki","CLEAR_SELECTION")

arcpy.SelectLayerByAttribute_management("jedn_budynki", "NEW_SELECTION", ''' "FID_jedn_funkc" = -1 ''')
arcpy.DeleteRows_management("jedn_budynki")
arcpy.SelectLayerByAttribute_management("jedn_budynki","CLEAR_SELECTION")

    # IZB
arcpy.Statistics_analysis("jedn_budynki", "Tabela_IZB", [["Pow_c_bud","SUM"]], ["OZN_JDN"])

arcpy.JoinField_management("jedn_funkc", "OZN_JDN", "Tabela_IZB", "OZN_JDN")
arcpy.CalculateField_management("jedn_funkc", "PowCB_SI", '!SUM_Pow_c_bud!', "PYTHON")
arcpy.DeleteField_management("jedn_funkc", ["OZN_JDN_1","SUM_Pow_c_bud","FREQUENCY"])

arcpy.CalculateField_management("jedn_funkc", "IZB_SI", '!PowCB_SI! / !Shape_Area!', "PYTHON")

    # SLK
arcpy.Statistics_analysis("jedn_budynki", "Tabela_SLK", [["liczba_kon_nadz","MEAN"]], ["OZN_JDN"])

arcpy.JoinField_management("jedn_funkc", "OZN_JDN", "Tabela_SLK", "OZN_JDN")
arcpy.CalculateField_management("jedn_funkc", "SLK_SI", '!MEAN_liczba_kon_nadz!', "PYTHON")
arcpy.DeleteField_management("jedn_funkc", ["OZN_JDN_1","MEAN_liczba_kon_nadz","FREQUENCY"])


# PNP

arcpy.Union_analysis(["ter_nieprzep", "jedn_funkc"], "jedn_nieprzep", "ALL")

arcpy.SelectLayerByAttribute_management("jedn_nieprzep", "NEW_SELECTION", ''' "FID_ter_nieprzep" = -1 ''')
arcpy.DeleteRows_management("jedn_nieprzep")
arcpy.SelectLayerByAttribute_management("jedn_nieprzep","CLEAR_SELECTION")

arcpy.SelectLayerByAttribute_management("jedn_nieprzep", "NEW_SELECTION", ''' "FID_jedn_funkc" = -1 ''')
arcpy.DeleteRows_management("jedn_nieprzep")
arcpy.SelectLayerByAttribute_management("jedn_nieprzep","CLEAR_SELECTION")

arcpy.Statistics_analysis("jedn_nieprzep", "Tabela_PNP", [["Shape_Area","SUM"]], ["OZN_JDN"])

arcpy.JoinField_management("jedn_funkc", "OZN_JDN", "Tabela_PNP", "OZN_JDN")
arcpy.CalculateField_management("jedn_funkc", "PowPNP_SI", '!SUM_Shape_Area!', "PYTHON")
arcpy.DeleteField_management("jedn_funkc", ["OZN_JDN_1","SUM_Shape_Area","FREQUENCY"])

arcpy.CalculateField_management("jedn_funkc", "PNP_SI", '(!PowPNP_SI! / !Shape_Area!) * 100', "PYTHON")
