# coding=utf-8
import arcpy

# arcpy.SelectLayerByAttribute_management("WG_Union", "NEW_SELECTION", """"OZN_JDN" = 'Ter_23129'""")
dict_of_CN = {'A': [0.000197269, 0.564393, 39.5299], 'B': [-0.000171445, 0.392418, 60.3064],
              'C': [0.0000418608, 0.232968, 74.1779], 'D': [0.000154441, 0.192297, 80.0777]}

def select_pnp(pole):
    # list_of_values = []
    where_clausule = """"ID_WG_UNION" = '{}'""".format(pole)
    arcpy.SelectLayerByAttribute_management("Tabela_WGPBC", "NEW_SELECTION", where_clausule)
    with arcpy.da.SearchCursor("Tabela_WGPBC", "*") as Cursor:
        selected_records = [[row[1], row[2], row[4]] for row in Cursor]
        print (selected_records)
        if len(selected_records) > 1:
            for pole in selected_records:
                rekord_index = selected_records.index(pole)
                if pole[1] == -1:
                    selected_records[rekord_index].append(1)
                    return selected_records[rekord_index]
                else:
                    return "coś poszło nie tak {}".format(selected_records)
        elif len(selected_records) == 1 and selected_records[0][1] == -1:
            selected_records[0].append(2)
            return selected_records[0]
        elif len(selected_records) == 1 and selected_records[0][1] == 1:
            selected_records[0].append(3)
            return selected_records[0]
        else:
            print("we have another value, we ve just finished on {}".format(selected_records[0][0]))


def calculate_CN(key_dict_from_table, prc_of_pnp):
    k = key_dict_from_table
    p = prc_of_pnp
    # print (dict_of_CN[k][0], dict_of_CN[k][1], dict_of_CN[k][2], k, p)
    calculate_CN_result = ((dict_of_CN[k][0] * p ** 2) + (dict_of_CN[k][1] * p) + dict_of_CN[k][2])
    return calculate_CN_result


with arcpy.da.UpdateCursor(fc, ["ID_WG_UNION", "Gleba_Area_PNP", "CN_gleba_ter_uz", "CzyPNP", "Grunt_WG_UNION_area",
                                "GrupaGleb"]) as Cursor:
    for row in Cursor:
        res = select_pnp(row[0])
        print("ok definicja wykonala sie")
        print(type(res), res[-1], res)
        if res[-1] == 1:
            print("res after 1")
            row[1] = res[2]
            row[3] = 1
            if row[5] in dict_of_CN.keys():
                key = row[5]
                proc_pnp = round(res[2] / row[4] * 100, 0)
                cn_result = calculate_CN(key, proc_pnp)
                row[2] = cn_result
            else:
                row[2] = None
            # Cursor.updateRow(row)
        elif res[-1] == 2:
            print("res after 2")
            row[1] = res[2]
            row[3] = 1
            if row[5] in dict_of_CN.keys():
                key = row[5]
                proc_pnp = 100
                cn_result = calculate_CN(key, proc_pnp)
                row[2] = cn_result
            else:
                row[2] = None
            # Cursor.updateRow(row)
        elif res[-1] == 3:
            print("res after 3")
            row[1] = res[2]
            row[3] = 0
            if row[5] in dict_of_CN.keys():
                key = row[5]
                proc_pnp = 0
                cn_result = calculate_CN(key, proc_pnp)
                row[2] = cn_result
            else:
                row[2] = None
            # Cursor.updateRow(row)
        else:
            print("coś poszło nie tak")
        Cursor.updateRow(row)
