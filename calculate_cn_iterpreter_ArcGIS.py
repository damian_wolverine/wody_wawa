# coding=utf-8
import arcpy

arcpy.env.workspace = r'E:\WODY_WAWA\DANE\BAZY_GIS\CN_2.gdb'
print ("pierwszy union")
arcpy.Union_analysis(["grunty", "wsk_urb_ter_uz"], "WG_Union")
arcpy.CopyFeatures_management("WG_Union", "WG_Union_copy")
field_delete_for_WG_Union = ["OBJECTID", "gridcode","Shape_Leng","SERIA","created_da","last_edi_1"]
arcpy.DeleteField_management("WG_Union", field_delete_for_WG_Union)
arcpy.AddField_management("WG_Union","ID_WG_UNION","TEXT","","","")
arcpy.AddField_management("WG_Union","Gleba_Area_PNP", "DOUBLE", "", "", "")
arcpy.AddField_management("WG_Union","CN_gleba_ter_uz", "DOUBLE", "", "", "")
# opcjonalnie dla sprawdzenia czy wartość pochodzi z PNP bezpośrednio czy obliczana z odwrotności PBC
# arcpy.AddField_management("WG_Union","CzyPNP", "SHORT", "", "", "")
arcpy.SelectLayerByAttribute_management("WG_Union", "NEW_SELECTION", """"FID_wsk_urb_ter_uz" = -1 or "FID_grunty" = -1 """)
arcpy.DeleteRows_management("WG_Union")
arcpy.CalculateField_management("WG_Union", "ID_WG_UNION", """"Idwg" + str(!OBJECTID_1!)""","PYTHON_9.3")
print ("drugi union z wg z pbc")
arcpy.Union_analysis(["WG_Union", "PBC_zielen"],"WGPBC_Union","ALL","#","GAPS")
arcpy.CopyFeatures_management("WGPBC_Union", "WGPBC_Union_copy")
arcpy.SelectLayerByAttribute_management("WGPBC_Union", "NEW_SELECTION", """"FID_WG_Union" = -1""")
arcpy.DeleteRows_management("WGPBC_Union")
arcpy.SelectLayerByAttribute_management("WGPBC_Union","NEW_SELECTION",""""FID_PBC_zielen" <> -1""")
arcpy.CalculateField_management("WGPBC_Union","FID_PBC_zielen","1","PYTHON_9.3")
arcpy.SelectLayerByAttribute_management("WGPBC_Union","CLEAR_SELECTION")
arcpy.DeleteField_management("WGPBC_Union","Shape_Leng")
print ("przechodzimy do statystyk")
arcpy.Statistics_analysis("WGPBC_Union","Tabela_WGPBC","Shape_Area SUM","ID_WG_UNION;FID_PBC_zielen")
arcpy.Statistics_analysis("Tabela_WGPBC","Tabela_WGPBC_sum_id_wg","FID_PBC_zielen COUNT","ID_WG_UNION")
arcpy.SelectLayerByAttribute_management("Tabela_WGPBC_sum_id_wg", "NEW_SELECTION", '''FREQUENCY <> COUNT_FID_PBC_zielen''') # pole powinny miec ta sama zawartość
arcpy.GetCount_management("Tabela_WGPBC_sum_id_wg") # result = 0
arcpy.SelectLayerByAttribute_management("Tabela_WGPBC_sum_id_wg", "NEW_SELECTION", """"COUNT_FID_PBC_zielen" > 2""")
arcpy.GetCount_management("Tabela_WGPBC_sum_id_wg")
arcpy.PivotTable_management(in_table="Tabela_WGPBC", fields="ID_WG_UNION", pivot_field="FID_PBC_zielen", value_field="SUM_Shape_Area", out_table="Tabela_WGPBC_pivotTab")
arcpy.JoinField_management("WG_Union", "ID_WG_UNION", "Tabela_WGPBC_pivotTab", "ID_WG_UNION", ["ID_WG_UNION", "FID_PBC_zielen_1", "FID_PBC_zielen1"])
arcpy.SelectLayerByAttribute_management("WG_Union", "NEW_SELECTION", """"ID_WG_Union_1" is null""")
result = int(arcpy.GetCount_management("WG_Union")[0]) # zwraca liczbe od 0 do n, gdzie n = liczba rekordów WG_Union
if result == 2:
    print ("warunek spelniony, przeliczany pole FID_PBC_zielen_1 is null")
    arcpy.DeleteRows_management("WG_Union")
elif result > 2:
    print ("jest więcej rekordów niepołączonych")
print ("print after if")

arcpy.SelectLayerByAttribute_management("WG_Union", "NEW_SELECTION", """"FID_PBC_zielen_1" = 0 and "FID_PBC_zielen1" = 0""")
# <Result 'WG_Union'>
arcpy.GetCount_management("WG_Union")
# <Result '0'>
# arcpy.SelectLayerByAttribute_management("WG_Union", "NEW_SELECTION", """"FID_PBC_zielen_1" > 0""")
# # <Result 'WG_Union'>
# arcpy.SelectLayerByAttribute_management("WG_Union", "NEW_SELECTION", """FID_PBC_zielen_1 <> FID_PBC_zielen1""")
# # <Result 'WG_Union'>
# arcpy.SelectLayerByAttribute_management("WG_Union", "NEW_SELECTION", """FID_PBC_zielen_1 = FID_PBC_zielen1""")
# # <Result 'WG_Union'>
# arcpy.GetCount_management("WG_Union")
# # <Result '0'>
arcpy.SelectLayerByAttribute_management("WG_Union", "NEW_SELECTION", """"Shape_Area" < FID_PBC_zielen_1""")
# <Result 'WG_Union'>
result = int(arcpy.GetCount_management("WG_Union")[0])
print (result)
if result > 0:
    arcpy.SelectLayerByAttribute_management("WG_Union", "SUBSET_SELECTION", """"FID_PBC_zielen1" > 0""")
    print(arcpy.GetCount_management("WG_Union")[0])
    arcpy.CalculateField_management("WG_Union", "FID_PBC_zielen1", "0", "PYTHON")
else:
    print ("table without values grather than 0")
print ("print after if")
# <Result '3719'>
arcpy.SelectLayerByAttribute_management("WG_Union", "NEW_SELECTION", """"Shape_Area" < FID_PBC_zielen1""")
# <Result 'WG_Union'>
result = int(arcpy.GetCount_management("WG_Union")[0])
print (result)
if result > 0:
    arcpy.SelectLayerByAttribute_management("WG_Union", "SUBSET_SELECTION", """"FID_PBC_zielen_1" > 0""")
    print(arcpy.GetCount_management("WG_Union")[0])
    # arcpy.CalculateField_management("WG_Union", "FID_PBC_zielen_1", "0", "PYTHON")
else:
    print ("table without values grather than 0")
print ("print after if")
# recalculation
arcpy.SelectLayerByAttribute_management("WG_Union", "NEW_SELECTION", """Shape_Area < FID_PBC_zielen_1""")
# <Result 'WG_Union'>
arcpy.SelectLayerByAttribute_management("WG_Union", "NEW_SELECTION", """Shape_Area < FID_PBC_zielen1""")
# <Result 'WG_Union'>

# calculate area CN for condition below:
arcpy.SelectLayerByAttribute_management("WG_Union", "CLEAR_SELECTION")
expression = "calculate(!FID_PBC_zielen_1!, !FID_PBC_zielen1!, !Shape_Area!)"
codeblock = """def calculate(zielen_1, zielen1, area):
    if zielen_1 == 0:
        return 0
    elif zielen1 == 0:
        return area
    elif zielen_1 > 0 and zielen1 > 0:
        return zielen_1
    else:
        return -9999"""

arcpy.CalculateField_management("WG_Union", "Gleba_Area_PNP", expression, "PYTHON_9.3", codeblock)

arcpy.SelectLayerByAttribute_management("WG_Union", "NEW_SELECTION", """"Gleba_Area_PNP" = 9999""")
# <Result 'WG_Union'>
arcpy.GetCount_management("WG_Union")
# <Result '0'>
arcpy.SelectLayerByAttribute_management("WG_Union", "NEW_SELECTION", """"Gleba_Area_PNP" is null""")
# <Result 'WG_Union'>
arcpy.GetCount_management("WG_Union")
# <Result '0'>
#######################################
# CN C A L C U L A T I O N for land use
#######################################
arcpy.SelectLayerByAttribute_management("WG_Union", "CLEAR_SELECTION")
expression = "calculateCN(!Gleba_CN!, !Gleba_Area_PNP!, !Shape_Area!)"

codeblock = """dict_of_CN = {'A': [0.000197269, 0.564393, 39.5299], 'B': [-0.000171445, 0.392418, 60.3064],
              'C': [0.0000418608, 0.232968, 74.1779], 'D': [0.000154441, 0.192297, 80.0777]}

def calculate_dikt_CN(key_dict_from_table, ar_pnp, ar_gleba):
    prc_of_pnp = round(ar_pnp / ar_gleba * 100, 0)
    calculate_CN_result = ((dict_of_CN[key_dict_from_table][0] * prc_of_pnp ** 2) + (dict_of_CN[key_dict_from_table][1] * prc_of_pnp) + dict_of_CN[key_dict_from_table][2])
    return calculate_CN_result

def calculateCN(glebaCN, area_pnp, area_gleba):
    if glebaCN in dict_of_CN.keys():
        key = glebaCN
        cn_result = calculate_dikt_CN(key, area_pnp, area_gleba)
        return cn_result
    elif glebaCN == 'Wody':
        return 0
    else:
        return -9999"""

arcpy.CalculateField_management("WG_Union", "CN_gleba_ter_uz", expression, "PYTHON_9.3", codeblock)
###################################
# write selected column to txt file
###################################
with arcpy.da.SearchCursor(fc, ls_fc) as Cursor:
    f = open(r'c:\Users\EKOVERT\Desktop\Damian\WODY_WAWA\GIT\wody_wawa\content_to_calc_CN.txt', 'a+')
    f.write("Id_dm,OZN_JDN,Shape_Area,ID_WG_UNION,CN_gleba_ter_uz,Shape_Area_1")
    for i in Cursor:
        string = ("{},{},{},{},{},{}".format(*i))
        f.write(string)
    f.close()

#########################################################
# Asign result of cn to CN_SI_PBC in layer wsk_urb_ter_uz
#########################################################
in_excel = r'c:\Users\EKOVERT\Desktop\Damian\WODY_WAWA\GIT\wody_wawa\Wynik_koncowy_CN_SI_2019.xlsx'
arcpy.ExcelToTable_conversion(in_excel, "Tabela_wynik_koncowy_CN", "Arkusz2")
arcpy.GetCount_management("Tabela_wynik_koncowy_CN")
# <Result '38960'>
arcpy.JoinField_management("wsk_urb_ter_uz", "OZN_JDN", "Tabela_wynik_koncowy_CN", "Obreb", "Cn")
# <Result 'wsk_urb_ter_uz'>
arcpy.SelectLayerByAttribute_management("wsk_urb_ter_uz", "NEW_SELECTION", """"Cn" is null""")
# <Result 'wsk_urb_ter_uz'>
arcpy.GetCount_management("wsk_urb_ter_uz")
# <Result '2'>
arcpy.CalculateField_management("wsk_urb_ter_uz", "Cn", "-9999","PYTHON_9.3")
# <Result 'wsk_urb_ter_uz'>
arcpy.SelectLayerByAttribute_management("wsk_urb_ter_uz", "CLEAR_SELECTION")
arcpy.CalculateField_management("wsk_urb_ter_uz", "CN_SI_PBC", "!Cn!","PYTHON_9.3")
arcpy.DeleteField_management("wsk_urb_ter_uz", "Cn")
# <Result 'wsk_urb_ter_uz'>