
# Skrypt do wyznaczania wartosci CN dla jednostek funkcjonalnych Warszawy - stan istniej?cy.

import arcpy 
arcpy.env.workspace = 'E:/Warszawa/ANALIZY_WERSJA_OST/ANALIZA_CN_JFUNKC/STAN_ISTNIEJ/W_ANALIZY_CN_JFUNKC.gdb'

field_delete = ["Geneza", "Litogeneza","Nr","OBJECTID","FUNKCJA","OPIS","OPIS_HYD","pow"]
field_delete_jedn = ["FID_pokrycie_CN", "FID_jedn_funkc", "FID_PG_Union", "FID_grunty"]
field_delete_tabela = ["FREQUENCY", "KLASA", "GRUPA_GLEB", "OZN_POKRYCIE", "WIERSZ_TAB", "WSP_CN"]
# KROK 1
# suma warstw, usuniecie niepotrzebnych pol, dodanie pola 'KLASA_PG', selekcja atrybutowa i nadpisanie warto?ci
arcpy.Union_analysis(["grunty", "pokrycie_CN"], "PG_Union", "ALL") 
#print "ok grunty pokrycie_CN"
# Pole GrupaGleb przyjmuje warto?ci: ('A', 'B', 'C', 'D', 'Wody') - rzutuje to na p?niejsze WYZNACZENIE warto?ci w polu 'KLASA_PG'
# Pole 'FID_grunty', warto??: -1 - okresla poligony, kt?rych nie by?o w wartwie grunty
# Pole 'FID_pokrycie_CN', warto??: -1 - okre?la poligony, kt?rych nie by?o w wartwie pokrycie_CN 
arcpy.DeleteField_management("PG_Union", field_delete)
#print "PG_UNION delete field ok"
# Pola kt?re pozosta?y: OBJECTID_1, Shape, FID_grunty, RodzajGrun, GrupaGleb, FID_pokrycie_CN, DZIELNICA, OZN_HYD, Shape_Length, Shape_Area, KLASA_PG
arcpy.AddField_management("PG_Union", "KLASA_PG", "TEXT")
# dodanie pola 'KLASA_PG'
arcpy.CalculateField_management("PG_Union", "KLASA_PG", 'str(!OZN_HYD!)+ !GrupaGleb!', "PYTHON")
#print "ok dodanie KLASA_PG oraz calc field"
# okre?lenie klasy obszaru (kombinacja liczbowo-literowa) - str. 5 dokumnetu 06_ANALIZA_PARAMETRU_CN.pdf
arcpy.SelectLayerByAttribute_management("PG_Union", "NEW_SELECTION", ''' "KLASA_PG" LIKE '17%' OR "KLASA_PG" LIKE '%Wody' ''')
arcpy.CalculateField_management("PG_Union", "KLASA_PG", '"17"')
arcpy.SelectLayerByAttribute_management("PG_Union","CLEAR_SELECTION")
#print "ok calc field after select, ok KROK 1"
# wyczyszczenie b??d?w - dostosowujemy to do danych referencyjnych w tabeli 'WART_CN'
#-------------
# KROK 2
# suma warstw, usuni?cie p?l dla FID poszczeg?lnych warstw, selekcja atrybutowa ('OZN_JDN'), usuni?cie zaznaczonych rekord?w, wyczyszczenie selekcji
arcpy.Union_analysis(["PG_Union", "Jedn_funkc"], "PGJF_Union", "ALL")
arcpy.DeleteField_management("PGJF_Union", field_delete_jedn)
# suma z warstw dla jednostek funkcjonalnyc oraz warstwy przygotowanej z grunt?w oraz pokrycie_CN
# usuni?cie p?l zb?dnych tj. z prefiksem 'FID'
#-------------
# KROK 3
# Czyszczenie shp 'PGJF_Union' z rekord?w o brakuj?ej warto?ci pola 'OZN_JDN', wyczysczenie selekcji
arcpy.SelectLayerByAttribute_management("PGJF_Union", "NEW_SELECTION", ''' "OZN_JDN" = '' ''')
arcpy.DeleteRows_management("PGJF_Union")
arcpy.SelectLayerByAttribute_management("PG_Union","CLEAR_SELECTION")
#--------------
# KROK 4
# summaryzacja, dodanie dodatkowych p?l, ?aczenie tabel, kalkulacja p?l, usuwanie p?l, selekcja rekord?w (?cinki przy sumie (polecenie Union) w KROKU 1 )
arcpy.Statistics_analysis("PGJF_Union", "Tabela_CN", [["Shape_Area","SUM"]], ["OZN_JDN","KLASA_PG"])
# summaryzacja powierzchni wg p?l: 'KLASA_PG', 'OZN_JDN'
arcpy.AddField_management("Tabela_CN", "WJF_CN", "DOUBLE")
arcpy.AddField_management("Tabela_CN", "JF_Area", "DOUBLE")
# utworzenie nowych p?l do przechowania inf. o CN i powierzchni pobranej z shp i tabeli (jedn_funkc, WART_CN)
arcpy.JoinField_management("Tabela_CN", "OZN_JDN", "jedn_funkc", "OZN_JDN")
arcpy.JoinField_management("Tabela_CN", "KLASA_PG", "WART_CN", "KLASA")
# przy??czenie tabel atrybut?w z "jedn_funkc", "WART_CN"
arcpy.CalculateField_management("Tabela_CN", "WJF_CN", '!WSP_CN!', "PYTHON")
arcpy.CalculateField_management("Tabela_CN", "JF_Area", '!Shape_Area!',"PYTHON")
# przeliczenie warto?ci p?l dla "WJF_CN", "JF_Area"
arcpy.DeleteField_management("Tabela_CN", field_delete_tabela)
# usuni?cie niepotrzebnych p?l ["FREQUENCY", "KLASA", "GRUPA_GLEB", "OZN_POKRYCIE", "WIERSZ_TAB", "WSP_CN"]
arcpy.SelectLayerByAttribute_management("Tabela_CN", "NEW_SELECTION", ''' "WJF_CN" IS NULL ''')
arcpy.DeleteRows_management("Tabela_CN")
arcpy.SelectLayerByAttribute_management("Tabela_CN","CLEAR_SELECTION")
# ostateczne usuni?cie skrawk?w po pierwszej sumie (polecenie Union KROK 1), b??dy w oznaczeniu CN nie ?acz? si? 

# Skrypt nie wykonuje ?redniej wa?onej dla CN w jednostkach funkcjonalnych, robi to makro w excelu
