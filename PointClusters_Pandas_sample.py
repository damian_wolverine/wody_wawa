import arcpy as ARCPY
import arcpy.management as DM
import os as OS
import sys as SYS
import subprocess as SUB

#### Parameter Dictionaries ####
clusterDict = {"KMEANS_HARTIGAN": "kmeansHartigan", "CLARA": "clara", 
               "B_CLUST": "bclust", "M_CLUST": "Mclust", 
               "KCCA_KMEANS": "kccaKmeans", 
               "CMEANS": "cmeans"} 

def PointClusters():
    #### Get User Provided Inputs ####
    inputFC = '"' + ARCPY.GetParameterAsText(0) + '"'
    outputFC = '"' + ARCPY.GetParameterAsText(1) + '"'
    numClusters = ARCPY.GetParameterAsText(2)
    clusterMethod = ARCPY.GetParameterAsText(3)
    clusterMethodStr = clusterDict[clusterMethod]
    varNames = ARCPY.GetParameterAsText(4)
    varNames = [ str(i) for i in varNames.split(";") ]
    varNames = ";".join(varNames)
    useLocation = ARCPY.GetParameterAsText(5)
    if useLocation == 'true':
        useLocation = "1"
    else:
        useLocation = "0"

    #### Create R Command ####
    pyScript = SYS.argv[0]
    toolDir = OS.path.dirname(pyScript)
    rScript = OS.path.join(toolDir, "PointClusters.r")
    rScript = '"' + rScript + '"'
    ARCPY.SetProgressor("default", "Executing R Script...")
    args = " ".join([inputFC, outputFC, 
                     numClusters, clusterMethodStr,
                     varNames, useLocation])
    RCMD = "R --slave --vanilla --args "
    cmd = RCMD + args + " < " + rScript

    #### Uncomment Next Line to Print Command ####
    #ARCPY.AddWarning(cmd)

    #### Execute Command ####
    OS.system(cmd)

    #### Project the Data ####
    # G.Turek: this code is wrong!! Commenting it out. See documentation
    # for this function on the web. Inputs should be a shapefile and .pro file
    # DM.DefineProjection(outputFC.strip('"'), inputFC.strip('"'))

    #### Render the Results ####
    params = ARCPY.gp.GetParameterInfo()
    renderFile = OS.path.join(toolDir, "RenderClusters.lyr")
    params[1].Symbology = renderFile

if __name__ == '__main__':

    test = PointClusters() 
