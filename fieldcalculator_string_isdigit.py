﻿def findNum(pole):
    res = [int(i) for i in pole.split() if i.isdigit()]
    if len(res) > 0:
        word = ''
        for i in res:
            word += str(i)
        return word
    else:
        return 0

#findNum(!Lokaliz!)
wynik = findNum ("Pustułeczki 20 (Ursynów)")
print(wynik)

# warunek do selekcji czy jest numer (wartość > 0, nie ma punktu adresowego nr 0)
# czy nie (wartość = 0)

def findUlica(pole):
    res = [int(i) for i in pole.split() if i.isdigit()]
    if len(res) > 0:
        cnt = 0
        for i in pole.split():
            print(i, cnt)
            if i.isdigit():
                return ' '.join(pole.split()[:cnt])
            else:
                cnt += 1
    else:
        return "brak"


import re
def czyNum(pole):
    if re.search("[0-9]", pole):
        return 1
    else:
        return 0

czyNum( !Lokaliz! )
