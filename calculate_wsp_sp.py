import arcpy
#arcpy.env.workspace =
# try to built try except contruct of all this code w some expectations what happens with our data

field_delete_for_GT_Union = ["OBJECTID", "gridcode","Shape_Leng","SERIA","created_da","last_edi_1"]
field_delete_zlew = ["OBJECTID_1", "ID", "ID_1", "GRIDCODE", "nazwa", "CODE_18", "Area_Ha", "nazwa_1", "NAZWA", "KLASA",]
field_delete_tabela = ["FREQUENCY", "KLASA", "GRUPA_GLEB", "OZN_POKRYCIE", "WIERSZ_TAB", "WSP_CN", "WSP_SP"]

arcpy.Union_analysis(["grunty", "ter_uzytk_bdot"], "GT_Union", "ALL")
# arcpy.DeleteField_management("GT_Union", field_delete_for_GT_Union)
arcpy.AddField_management("GT_Union", "KLASA_GT", "TEXT")
arcpy.CalculateField_management("GT_Union", "KLASA_GT", 'str(!OZN_HYD!)+ !Gleba_CN!', "PYTHON")

# arcpy.SelectLayerByAttribute_management("GT_Union", "NEW_SELECTION", """"FID_grunty" = -1 OR "FID_ter_uzytk_bdot" = -1""")
# arcpy.DeleteRows_management("GT_Union")

arcpy.SelectLayerByAttribute_management("GT_Union", "NEW_SELECTION", """"KLASA_GT" LIKE '17%' OR "KLASA_GT" LIKE '%Wody'""")
arcpy.CalculateField_management("GT_Union", "KLASA_GT", "17", "PYTHON_9.3")
arcpy.SelectLayerByAttribute_management("GT_Union","CLEAR_SELECTION")

print ("drugi union z granicami zlewni")
arcpy.CopyFeatures_management("GT_Union", "GT_Union_copy")
arcpy.Union_analysis(["GT_Union", "wsk_urb_ter_uz"], "GTWSK_Union", "ALL")
# arcpy.SelectLayerByAttribute_management("GTWSK_Union", "NEW_SELECTION", """"FID_GT_Union" = -1 OR "FID_wsk_urb_ter_uz" = -1""")
# print(arcpy.GetCount_management("GTWSK_Union"))
# expected 0 if not we need to check fid for GT_Union and fid for wsk_urb_ter_uz and check
# area of polygons, actual we have 4 small polygons
# arcpy.DeleteRows_management("GTWSK_Union")
# # cleaning atribute table
# arcpy.DeleteField_management("GTWSK_Union", field_delete_zlew)
#
arcpy.SelectLayerByAttribute_management("GTWSK_Union", "NEW_SELECTION", """"OZN_JDN" is null or "OZN_JDN" = ''""")
print(arcpy.GetCount_management("GTWSK_Union"))
arcpy.DeleteRows_management("GTWSK_Union")
arcpy.SelectLayerByAttribute_management("GTWSK_Union","CLEAR_SELECTION")
#
field_delete_gtclczl = ["area", "Shape_Leng", "FID_wsk_urb_ter_uz"]
arcpy.DeleteField_management("GTWSK_Union", field_delete_gtclczl)

print ("przechodzimy do statystyk")
arcpy.Statistics_analysis("GTWSK_Union", "Tabela_SP", [["Shape_Area","SUM"]], ["OZN_JDN","KLASA_GT"])

arcpy.AddField_management("Tabela_SP", "WSK_SP", "DOUBLE")
arcpy.AddField_management("Tabela_SP", "WSK_Area", "DOUBLE")

arcpy.JoinField_management("Tabela_SP", "OZN_JDN", "wsk_urb_ter_uz", "OZN_JDN")
arcpy.JoinField_management("Tabela_SP", "KLASA_GT", "WART_SP", "KLASA")

arcpy.CalculateField_management("Tabela_SP", "WSK_SP", '!WSP_SP!', "PYTHON")
arcpy.CalculateField_management("Tabela_SP", "WSK_Area", '!Shape_Area!',"PYTHON")

#arcpy.DeleteField_management("Tabela_SP", field_delete_tabela)

arcpy.SelectLayerByAttribute_management("Tabela_SP", "NEW_SELECTION", """"WSK_SP" is null""")
print(arcpy.GetCount_management("Tabela_SP"))
arcpy.DeleteRows_management("Tabela_SP")
arcpy.SelectLayerByAttribute_management("Tabela_SP","CLEAR_SELECTION")

# export content for calculate SP in excel
field_content_sp = ['OBJECTID', 'OZN_JDN', 'KLASA_GT', 'SUM_Shape_Area', 'WSK_SP', 'WSK_Area']
with arcpy.da.SearchCursor("Tabela_SP", field_content_sp) as Cursor:
    f = open(r'E:\WODY_WAWA\SKRYPTY\content_to_calc_SP.txt', 'a+')
    f.write("OBJECTID,OZN_JDN,KLASA_GT,SUM_Shape_Area,WSK_SP,WSK_Area\n")
    for i in Cursor:
        string = ("{},{},{},{},{},{}\n".format(*i))
        f.write(string)
    f.close()

#########################################################
# Asign result of SP to CN_SI_PBC in layer wsk_urb_ter_uz
#########################################################

in_excel = r'E:\WODY_WAWA\SKRYPTY\Wynik_koncowy_SP_2019.xlsx'
arcpy.ExcelToTable_conversion(in_excel, "Tabela_wynik_koncowy_SP", "Arkusz2")
print(arcpy.GetCount_management("Tabela_wynik_koncowy_SP"))
# <Result '38960'>
arcpy.JoinField_management("wsk_urb_ter_uz", "OZN_JDN", "Tabela_wynik_koncowy_SP", "Obreb", "SP")
# <Result 'wsk_urb_ter_uz'>
arcpy.SelectLayerByAttribute_management("wsk_urb_ter_uz", "NEW_SELECTION", """"SP" is null""")
# <Result 'wsk_urb_ter_uz'>
print(arcpy.GetCount_management("wsk_urb_ter_uz"))
# <Result '2'>
# calculate if SP is null
arcpy.CalculateField_management("wsk_urb_ter_uz", "SP", "-9999","PYTHON_9.3")
# <Result 'wsk_urb_ter_uz'>
# arcpy.SelectLayerByAttribute_management("wsk_urb_ter_uz", "CLEAR_SELECTION")
# arcpy.AddField_management("wsk_urb_ter_uz", "SP", "DOUBLE", "", "", "")
# arcpy.CalculateField_management("wsk_urb_ter_uz", "SP", "!SP!","PYTHON_9.3")
# arcpy.DeleteField_management("wsk_urb_ter_uz", "SP")
# <Result 'wsk_urb_ter_uz'>