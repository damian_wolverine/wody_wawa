# coding=utf-8
import arcpy

arcpy.env.workspace = r'E:\WODY_WAWA\DANE\BAZY_GIS\CN_2.gdb'
print ("pierwszy union")
arcpy.Union_analysis(["grunty", "zlewnie_do_granicy_wawy"], "GZ_Union")
arcpy.CopyFeatures_management("GZ_Union", "GZ_Union_copy")
field_delete_for_GZ_Union = ["Geneza", "Nr","Litogeneza","RodzajGrun", "ID", "pow_ha", "row"]
arcpy.DeleteField_management("GZ_Union", field_delete_for_GZ_Union)
arcpy.AddField_management("GZ_Union","ID_GZ_UNION","TEXT","","","")
arcpy.AddField_management("GZ_Union","Gleba_Area_PNP", "DOUBLE", "", "", "")
arcpy.AddField_management("GZ_Union","CN_gleba_river_basin", "DOUBLE", "", "", "")
arcpy.CalculateField_management("GZ_Union", "ID_GZ_UNION", """"Idgz" + str(!OBJECTID_1!)""","PYTHON_9.3")
arcpy.SelectLayerByAttribute_management("GZ_Union", "NEW_SELECTION", """"FID_zlewnie_do_granicy_wawy" = -1 or "FID_grunty" = -1 """)
arcpy.DeleteRows_management("GZ_Union")
print ("drugi union z wg z pbc")
arcpy.Union_analysis(["GZ_Union", "PBC_zielen"],"GZPBC_Union","ALL","#","GAPS")
arcpy.CopyFeatures_management("GZPBC_Union", "GZPBC_Union_copy")
arcpy.SelectLayerByAttribute_management("GZPBC_Union", "NEW_SELECTION", """"FID_GZ_Union" = -1""")
arcpy.DeleteRows_management("GZPBC_Union")
arcpy.SelectLayerByAttribute_management("GZPBC_Union","NEW_SELECTION",""""FID_PBC_zielen" <> -1""")
arcpy.CalculateField_management("GZPBC_Union","FID_PBC_zielen","1","PYTHON_9.3")
arcpy.SelectLayerByAttribute_management("GZPBC_Union","CLEAR_SELECTION")
print ("przechodzimy do statystyk")
arcpy.Statistics_analysis("GZPBC_Union","Tabela_GZPBC","Shape_Area SUM","ID_GZ_UNION;FID_PBC_zielen")
arcpy.Statistics_analysis("Tabela_GZPBC","Tabela_GZPBC_sum_id_wg","FID_PBC_zielen COUNT","ID_GZ_UNION")
arcpy.SelectLayerByAttribute_management("Tabela_GZPBC_sum_id_wg", "NEW_SELECTION", '''FREQUENCY <> COUNT_FID_PBC_zielen''') # pole powinny miec ta sama zawartość
print(arcpy.GetCount_management("Tabela_GZPBC_sum_id_wg")[0]) # result = 0
arcpy.SelectLayerByAttribute_management("Tabela_GZPBC_sum_id_wg", "NEW_SELECTION", """"COUNT_FID_PBC_zielen" > 2""")
print(arcpy.GetCount_management("Tabela_GZPBC_sum_id_wg")[0])
arcpy.PivotTable_management(in_table="Tabela_GZPBC", fields="ID_GZ_UNION", pivot_field="FID_PBC_zielen", value_field="SUM_Shape_Area", out_table="Tabela_GZPBC_pivotTab")
arcpy.JoinField_management("GZ_Union", "ID_GZ_UNION", "Tabela_GZPBC_pivotTab", "ID_GZ_UNION", ["ID_GZ_UNION", "FID_PBC_zielen_1", "FID_PBC_zielen1"])
arcpy.SelectLayerByAttribute_management("GZ_Union", "NEW_SELECTION", """"ID_GZ_Union_1" is null""")
result = int(arcpy.GetCount_management("GZ_Union")[0]) # zwraca liczbe od 0 do n, gdzie n = liczba rekordów GZ_Union
if result == 2:
    print ("warunek spelniony, przeliczany pole FID_PBC_zielen_1 is null")
    # arcpy.DeleteRows_management("GZ_Union")
elif result > 2:
    print ("jest więcej rekordów niepołączonych")
print ("print after if")

arcpy.SelectLayerByAttribute_management("GZ_Union", "NEW_SELECTION", """"FID_PBC_zielen_1" = 0 and "FID_PBC_zielen1" = 0""")
# <Result 'GZ_Union'>
print (arcpy.GetCount_management("GZ_Union")[0])
arcpy.SelectLayerByAttribute_management("GZ_Union", "NEW_SELECTION", """"Shape_Area" <= FID_PBC_zielen_1""")
# <Result 'GZ_Union'>
result = int(arcpy.GetCount_management("GZ_Union")[0])
print ("We calculate field pnp, result of comparison with Shape area is: {}".format(result))
if result > 0:
    print ("we come in if statement")
    arcpy.SelectLayerByAttribute_management("GZ_Union", "SUBSET_SELECTION", """"FID_PBC_zielen1" > 0""")
    result_reselect = int(arcpy.GetCount_management("GZ_Union")[0])
    if result_reselect > 0:
        print("number of record grather than 0 in opposite field pbc: {}".format(result_reselect))
        # arcpy.CalculateField_management("GZ_Union", "FID_PBC_zielen1", "0", "PYTHON")
    else:
        print ("opposite field pbc without values grater than 0")
else:
    print ("table without values of pnp grater than Shape area")
print ("print after if")
# <Result '3719'>
arcpy.SelectLayerByAttribute_management("GZ_Union", "NEW_SELECTION", """"Shape_Area" <= FID_PBC_zielen1""")
# <Result 'GZ_Union'>
result = int(arcpy.GetCount_management("GZ_Union")[0])
print ("We calculate field pbc, result of comparison with Shape area is: {}".format(result))
if result > 0:
    print ("we come in if statement")
    arcpy.SelectLayerByAttribute_management("GZ_Union", "SUBSET_SELECTION", """"FID_PBC_zielen_1" > 0""")
    result_reselect = int(arcpy.GetCount_management("GZ_Union")[0])
    if result_reselect > 0:
        print("number of record grather than 0 in opposite field pnp: {}".format(result_reselect))
        # arcpy.CalculateField_management("GZ_Union", "FID_PBC_zielen1", "0", "PYTHON")
    else:
        print ("opposite field pnp without values grater than 0")
else:
    print ("table without values of pbc grater than Shape area")
print ("print after if")
# recalculation
arcpy.SelectLayerByAttribute_management("GZ_Union", "NEW_SELECTION", """Shape_Area < FID_PBC_zielen_1""")
print(arcpy.GetCount_management("GZ_Union")[0])
# <Result 'GZ_Union'>
arcpy.SelectLayerByAttribute_management("GZ_Union", "NEW_SELECTION", """Shape_Area < FID_PBC_zielen1""")
print(arcpy.GetCount_management("GZ_Union")[0])
# <Result 'GZ_Union'>

# calculate area CN for condition below:
arcpy.SelectLayerByAttribute_management("GZ_Union", "CLEAR_SELECTION")
expression = "calculate(!FID_PBC_zielen_1!, !FID_PBC_zielen1!, !Shape_Area!)"
codeblock = """def calculate(zielen_1, zielen1, area):
    if zielen_1 == 0:
        return 0
    elif zielen1 == 0:
        return area
    elif zielen_1 > 0 and zielen1 > 0:
        return zielen_1
    else:
        return -9999"""

arcpy.CalculateField_management("GZ_Union", "Gleba_Area_PNP", expression, "PYTHON_9.3", codeblock)

arcpy.SelectLayerByAttribute_management("GZ_Union", "NEW_SELECTION", """"Gleba_Area_PNP" = -9999""")
print(arcpy.GetCount_management("GZ_Union"))
# <Result '0'>
arcpy.SelectLayerByAttribute_management("GZ_Union", "NEW_SELECTION", """"Gleba_Area_PNP" is null""")
print(arcpy.GetCount_management("GZ_Union"))
# <Result '0'>
#######################################
# CN C A L C U L A T I O N for land use
#######################################
arcpy.SelectLayerByAttribute_management("GZ_Union", "CLEAR_SELECTION")
expression = "calculateCN(!Gleba_CN!, !Gleba_Area_PNP!, !Shape_Area!)"

codeblock = """dict_of_CN = {'A': [0.000197269, 0.564393, 39.5299], 'B': [-0.000171445, 0.392418, 60.3064],
              'C': [0.0000418608, 0.232968, 74.1779], 'D': [0.000154441, 0.192297, 80.0777]}

def calculate_dikt_CN(key_dict_from_table, ar_pnp, ar_gleba):
    prc_of_pnp = round(ar_pnp / ar_gleba * 100, 0)
    calculate_CN_result = ((dict_of_CN[key_dict_from_table][0] * prc_of_pnp ** 2) + (dict_of_CN[key_dict_from_table][1] * prc_of_pnp) + dict_of_CN[key_dict_from_table][2])
    return calculate_CN_result

def calculateCN(glebaCN, area_pnp, area_gleba):
    if glebaCN in dict_of_CN.keys():
        key = glebaCN
        cn_result = calculate_dikt_CN(key, area_pnp, area_gleba)
        return cn_result
    elif glebaCN == 'Wody':
        return 0
    else:
        return -9999"""

arcpy.CalculateField_management("GZ_Union", "CN_gleba_river_basin", expression, "PYTHON_9.3", codeblock)
arcpy.SelectLayerByAttribute_management("GZ_Union", "NEW_SELECTION", """"CN_gleba_river_basin" = -9999""")
print(arcpy.GetCount_management("GZ_Union"))
# check weather CN_gleba_river_basin has value grateher or equal 101
arcpy.SelectLayerByAttribute_management("GZ_Union", "NEW_SELECTION", """"CN_gleba_river_basin" >= 101""")
print(arcpy.GetCount_management("GZ_Union"))
arcpy.SelectLayerByAttribute_management("GZ_Union", "NEW_SELECTION", """"CN_gleba_river_basin" > 100""")
arcpy.CalculateField_management("GZ_Union", "CN_gleba_river_basin", "100", "PYTHON_9.3")
# join shape area from zlewnie_do_granicy_wawy
arcpy.SelectLayerByAttribute_management("GZ_Union", "CLEAR_SELECTION")
# tutaj dołaczenie poprzez narzedzie merge danych z CLC_Union_clip i utworzenie GZCLC_Union
arcpy.JoinField_management("GZCLC_Union", "ZlewShort", "zlewnie_Ursynow_2_v2_topology", "ZlewShort", ["ZlewShort", "Shape_Area"])
arcpy.SelectLayerByAttribute_management("GZ_Union", "NEW_SELECTION", """"ZlewShort_1" is null""")
# <Result 'GZ_Union'>
print(arcpy.GetCount_management("GZ_Union")[0])
# <Result '0'>
###################################
# write selected column to txt file
###################################
fc = 'GZCLC_Union'
ls_fc = ["OBJECTID_1", "ZlewShort", "ID_GZ_UNION", "Shape_Area", "CN_gleba_river_basin", "Shape_Area_1"]
with arcpy.da.SearchCursor(fc, ls_fc) as Cursor:
    f = open(r'c:\Users\EKOVERT\Desktop\Damian\WODY_WAWA\GIT\wody_wawa\content_to_calc_CN_river_basin_ursynow.txt', 'a+')
    f.write("OBJECTID_1,ZlewShort,ID_GZ_UNION,Shape_Area,CN_gleba_river_basin,Shape_Area_1\n")
    for i in Cursor:
        string = ("{},{},{},{},{},{}\n".format(*i))
        f.write(string)
    f.close()

#########################################################
# Asign result of cn to CN_SI_PBC in layer zlewnie_Ursynow_2_v2_topology
#########################################################
in_excel = r'C:\Users\EKOVERT\Desktop\Damian\WODY_WAWA\GIT\wody_wawa\Wynik_koncowy_CN_zlew_ursynow_2019.xlsx'
arcpy.ExcelToTable_conversion(in_excel, "Tabela_wynik_koncowy_CN_zlew", "Arkusz2")
print(arcpy.GetCount_management("Tabela_wynik_koncowy_CN_zlew"))
# <Result '38960'>
arcpy.JoinField_management("zlewnie_Ursynow_2_v2_topology", "ZlewShort", "Tabela_wynik_koncowy_CN_zlew", "Obreb", "Cn")
# <Result 'zlewnie_Ursynow_2_v2_topology'>
arcpy.SelectLayerByAttribute_management("zlewnie_Ursynow_2_v2_topology", "NEW_SELECTION", """"Cn" is null""")
# <Result 'zlewnie_Ursynow_2_v2_topology'>
print(arcpy.GetCount_management("zlewnie_Ursynow_2_v2_topology"))
# <Result '2'>
# calculate if CN is null
# arcpy.CalculateField_management("zlewnie_Ursynow_2_v2_topology", "Cn", "-9999","PYTHON_9.3")
# <Result 'zlewnie_Ursynow_2_v2_topology'>
arcpy.SelectLayerByAttribute_management("zlewnie_Ursynow_2_v2_topology", "CLEAR_SELECTION")
arcpy.AddField_management("zlewnie_Ursynow_2_v2_topology", "CN_SI_PBC", "DOUBLE", "", "", "")
arcpy.CalculateField_management("zlewnie_Ursynow_2_v2_topology", "CN_SI_PBC", "!Cn!","PYTHON_9.3")
arcpy.DeleteField_management("zlewnie_Ursynow_2_v2_topology", "Cn")
# <Result 'zlewnie_Ursynow_2_v2_topology'>