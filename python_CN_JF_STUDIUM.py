﻿
# Skrypt do wyznaczania wartosci CN dla jednostek funkcjonalnych Warszawy - kierunki ze studium.

import arcpy 
arcpy.env.workspace = 'E:/Warszawa/ANALIZY_WERSJA_OST/ANALIZA_CN_JFUNKC/KIERUNKI/W_ANALIZY_CN_JF_STUDIUM.gdb'

field_delete = ["Geneza", "Litogeneza","Nr","OBJECTID","LEGENDA","RODZ_POK"]
field_delete_jedn = ["FID_pokrycie_SUiKZP", "FID_jedn_funkc", "FID_PG_Union", "FID_grunty"]
field_delete_tabela = ["FREQUENCY", "KLASA", "GRUPA_GLEB", "OZN_POKRYCIE", "WIERSZ_TAB", "WSP_CN"]
# KROK 1
# suma warstw, usuniecie niepotrzebnych pol, dodanie pola 'KLASA_PG', selekcja atrybutowa i nadpisanie wartości
arcpy.Union_analysis(["grunty", "pokrycie_SUiKZP"], "PG_Union", "ALL")
#print "ok grunty pokrycie_CN"
# Pole GrupaGleb przyjmuje wartości: ('A', 'B', 'C', 'D', 'Wody') - rzutuje to na póżniejsze WYZNACZENIE wartości w polu 'KLASA_PG'
# Pole 'FID_grunty', wartość: -1 - okresla poligony, których nie było w wartwie grunty
# Pole 'FID_pokrycie_CN', wartość: -1 - określa poligony, których nie było w wartwie pokrycie_CN 
arcpy.DeleteField_management("PG_Union", field_delete)
#print "PG_UNION delete field ok"
# Pola które pozostały: OBJECTID_1, Shape, FID_grunty, RodzajGrun, GrupaGleb, FID_pokrycie_CN, DZIELNICA, OZN_HYD, Shape_Length, Shape_Area, KLASA_PG
arcpy.AddField_management("PG_Union", "KLASA_PG", "TEXT")
# dodanie pola 'KLASA_PG'
arcpy.CalculateField_management("PG_Union", "KLASA_PG", 'str(!OZN_LEG!)+ !GrupaGleb!', "PYTHON")
#print "ok dodanie KLASA_PG oraz calc field"
# określenie klasy obszaru (kombinacja liczbowo-literowa) - str. 5 dokumnetu 06_ANALIZA_PARAMETRU_CN.pdf
arcpy.SelectLayerByAttribute_management("PG_Union", "NEW_SELECTION", ''' "KLASA_PG" LIKE '17%' OR "KLASA_PG" LIKE '%Wody' ''')
arcpy.CalculateField_management("PG_Union", "KLASA_PG", '"17"')
arcpy.SelectLayerByAttribute_management("PG_Union","CLEAR_SELECTION")
#print "ok calc field after select, ok KROK 1"
# wyczyszczenie błędów - dostosowujemy to do danych referencyjnych w tabeli 'WART_CN'
#-------------
# KROK 2
arcpy.Union_analysis(["PG_Union", "jedn_funkc"], "PGJF_Union", "ALL")
arcpy.DeleteField_management("PGJF_Union", field_delete_jedn)
# KROK 3
# Czyszczenie shp 'PGJF_Union' z rekordów o brakująej wartości pola 'OZN_JDN', wyczysczenie selekcji
arcpy.SelectLayerByAttribute_management("PGJF_Union", "NEW_SELECTION", ''' "OZN_JDN" = '' ''')
arcpy.DeleteRows_management("PGJF_Union")
arcpy.SelectLayerByAttribute_management("PG_Union","CLEAR_SELECTION")
#--------------
# KROK 4
# summaryzacja, dodanie dodatkowych pól, łaczenie tabel, kalkulacja pól, usuwanie pól, selekcja rekordów (ścinki przy sumie (polecenie Union) w KROKU 1 )
arcpy.Statistics_analysis("PGJF_Union", "Tabela_CN", [["Shape_Area","SUM"]], ["OZN_JDN","KLASA_PG"])
# summaryzacja powierzchni wg pól: 'KLASA_PG', 'OZN_JDN'
arcpy.AddField_management("Tabela_CN", "WJF_CN_ST", "LONG")
arcpy.AddField_management("Tabela_CN", "JF_Area", "DOUBLE")
# utworzenie nowych pól do przechowania inf. o CN i powierzchni pobranej z shp i tabeli (jedn_funkc, WART_CN)
arcpy.JoinField_management("Tabela_CN", "OZN_JDN", "jedn_funkc", "OZN_JDN")
arcpy.JoinField_management("Tabela_CN", "KLASA_PG", "WART_CN", "KLASA")
# przyłączenie tabel atrybutów z "jedn_funkc", "WART_CN"
arcpy.CalculateField_management("Tabela_CN", "WJF_CN_ST", '!WSP_CN!', "PYTHON")
arcpy.CalculateField_management("Tabela_CN", "JF_Area", '!Shape_Area!',"PYTHON")
# przeliczenie wartości pól dla "WJF_CN", "JF_Area"
arcpy.DeleteField_management("Tabela_CN", field_delete_tabela)
# usunięcie niepotrzebnych pól ["FREQUENCY", "KLASA", "GRUPA_GLEB", "OZN_POKRYCIE", "WIERSZ_TAB", "WSP_CN"]
arcpy.SelectLayerByAttribute_management("Tabela_CN", "NEW_SELECTION", ''' "WJF_CN_ST" IS NULL ''')
arcpy.DeleteRows_management("Tabela_CN")
arcpy.SelectLayerByAttribute_management("Tabela_CN","CLEAR_SELECTION")
# ostateczne usunięcie skrawków po pierwszej sumie (polecenie Union KROK 1), błędy w oznaczeniu CN nie łaczą się 

# Skrypt nie wykonuje średniej ważonej dla CN w jednostkach funkcjonalnych, robi to makro w excelu


