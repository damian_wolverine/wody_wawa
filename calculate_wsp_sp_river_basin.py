import arcpy
#arcpy.env.workspace =
# try to built try except contruct of all this code w some expectations what happens with our data

field_delete_for_GT_Union = ["OBJECTID", "gridcode","Shape_Leng","SERIA","created_da","last_edi_1"]
field_delete_zlew = ["OBJECTID_1", "ID", "ID_1", "GRIDCODE", "nazwa", "CODE_18", "Area_Ha", "nazwa_1", "NAZWA", "KLASA",]
field_delete_tabela = ["FREQUENCY", "KLASA", "GRUPA_GLEB", "OZN_POKRYCIE", "WIERSZ_TAB", "WSP_CN", "WSP_SP"]

arcpy.Union_analysis(["grunty", "ter_uzytk_bdot"], "GT_Union", "ALL")
# arcpy.DeleteField_management("GT_Union", field_delete_for_GT_Union)
arcpy.AddField_management("GT_Union", "KLASA_GT", "TEXT")
arcpy.CalculateField_management("GT_Union", "KLASA_GT", 'str(!OZN_HYD!)+ !Gleba_CN!', "PYTHON")

arcpy.SelectLayerByAttribute_management("GT_Union", "NEW_SELECTION", """"FID_grunty" = -1 OR "FID_ter_uzytk_bdot" = -1""")
arcpy.DeleteRows_management("GT_Union")

arcpy.SelectLayerByAttribute_management("GT_Union", "NEW_SELECTION", """"KLASA_GT" LIKE '17%' OR "KLASA_GT" LIKE '%Wody'""")
arcpy.CalculateField_management("GT_Union", "KLASA_GT", "17", "PYTHON_9.3")
arcpy.SelectLayerByAttribute_management("GT_Union","CLEAR_SELECTION")

# dla dołączamy powierzchnie z corina
arcpy.Union_analysis(["GT_Union", "clc18_PL_clip"], "GTCLC_Union", "ALL")
# for check that all numbers is into
# advanced we should join values not only check correspondence of records between layers
arcpy.SelectLayerByAttribute_management("clc18_PL_clip", "CLEAR_SELECTION")
result_get_cnt_clc_records = arcpy.GetCount_management("clc18_PL_clip")[0]
arcpy.SelectLayerByAttribute_management("GTCLC_Union", "NEW_SELECTION", """"FID_clc18_PL_clip" >= 0""")
result_get_cnt_gtclc_union = arcpy.GetCount_management("GTCLC_Union")[0]
if result_get_cnt_clc_records == result_get_cnt_gtclc_union:
    print ("ok, clc records is: {} and is equal to gtclc_unoin: {}".format(result_get_cnt_clc_records, result_get_cnt_gtclc_union))
else:
    print ("we had a problem clc ({} records) is different then gtclc_union ({} records)".format(result_get_cnt_clc_records, result_get_cnt_gtclc_union))
arcpy.SelectLayerByAttribute_management("GTCLC_Union", "NEW_SELECTION", """"FID_GT_Union" >= 0 and "FID_clc18_PL_clip" >= 0""")
# <Result 'GTCLC_Union'>
print(arcpy.GetCount_management("GTCLC_Union")[0])
# expected 0
# fill rest of null records in field "KLASA_GT" form clc18_PL_clip (field 'CODE_18')
arcpy.SelectLayerByAttribute_management("GTCLC_Union", "NEW_SELECTION", """"FID_clc18_PL_clip" >= 0""")
arcpy.CalculateField_management("GTCLC_Union", "KLASA_GT", "!CODE_18!", "PYTHON_9.3")
# check field KLASA_GT it should't be null
arcpy.SelectLayerByAttribute_management("GTCLC_Union", "NEW_SELECTION", """"KLASA_GT" is null""")
# <Result 'GTCLC_Union'>
print(arcpy.GetCount_management("GTCLC_Union"))
# expected 0
arcpy.JoinField_management("GTCLC_Union", "KLASA_GT", "WART_SP", "KLASA", ["KLASA", "WSP_SP"])
arcpy.SelectLayerByAttribute_management("GTCLC_Union", "NEW_SELECTION", """"WSP_SP" is null""")
# <Result 'GTCLC_Union'>
print(arcpy.GetCount_management("GTCLC_Union"))
# expected 0
# ---------------------------

print ("drugi union z granicami zlewni")
arcpy.CopyFeatures_management("GTCLC_Union", "GTCLC_Union_copy")
arcpy.Union_analysis(["GTCLC_Union", "zlewnie_warszawa_podzial_KW"], "GTCLCZL_Union", "ALL")
arcpy.SelectLayerByAttribute_management("GTCLCZL_Union", "NEW_SELECTION", """"FID_GTCLC_Union" = -1 OR "FID_zlewnie_warszawa_podzial_KW" = -1""")
print(arcpy.GetCount_management("GTCLCZL_Union"))
# expected 0 if not we need to check fid for GTCLC_Union and fid for zlewnie_warszawa_podzial_KW and check
# area of polygons, actual we have 4 small polygons
arcpy.DeleteRows_management("GTCLCZL_Union")
# cleaning atribute table
arcpy.DeleteField_management("GTCLCZL_Union", field_delete_zlew)
#
arcpy.SelectLayerByAttribute_management("GTCLCZL_Union", "NEW_SELECTION", """"ZlewShort" is null or "ZlewShort" = ''""")
print(arcpy.GetCount_management("GTCLCZL_Union"))
arcpy.DeleteRows_management("GTCLCZL_Union")
arcpy.SelectLayerByAttribute_management("GTCLCZL_Union","CLEAR_SELECTION")
#
field_delete_gtclczl = ["area", "Shape_Leng", "FID_zlewnie_warszawa_podzial_KW"]
arcpy.DeleteField_management("GTCLCZL_Union", field_delete_gtclczl)

print ("przechodzimy do statystyk")
arcpy.Statistics_analysis("GTCLCZL_Union", "Tabela_SP", [["Shape_Area","SUM"]], ["ZlewShort","KLASA_GT"])

arcpy.AddField_management("Tabela_SP", "ZL_SP", "DOUBLE")
arcpy.AddField_management("Tabela_SP", "ZL_Area", "DOUBLE")

arcpy.JoinField_management("Tabela_SP", "ZlewShort", "zlewnie_warszawa_podzial_KW", "ZlewShort")
arcpy.JoinField_management("Tabela_SP", "KLASA_GT", "WART_SP", "KLASA")

arcpy.CalculateField_management("Tabela_SP", "ZL_SP", '!WSP_SP!', "PYTHON")
arcpy.CalculateField_management("Tabela_SP", "ZL_Area", '!Shape_Area!',"PYTHON")

#arcpy.DeleteField_management("Tabela_SP", field_delete_tabela)

arcpy.SelectLayerByAttribute_management("Tabela_SP", "NEW_SELECTION", """"ZL_SP" is null""")
print(arcpy.GetCount_management("Tabela_SP"))
arcpy.DeleteRows_management("Tabela_SP")
arcpy.SelectLayerByAttribute_management("Tabela_SP","CLEAR_SELECTION")

# export content for calculate SP in excel
field_content_sp = ['OBJECTID', 'ZlewShort', 'KLASA_GT', 'SUM_Shape_Area', 'ZL_SP', 'ZL_Area']
with arcpy.da.SearchCursor("Tabela_SP", field_content_sp) as Cursor:
    f = open(r'E:\WODY_WAWA\SKRYPTY\content_to_calc_SP_zlew_v2.txt', 'a+')
    f.write("OBJECTID,ZlewShort,KLASA_GT,SUM_Shape_Area,ZL_SP,ZL_Area\n")
    for i in Cursor:
        string = ("{},{},{},{},{},{}\n".format(*i))
        f.write(string)
    f.close()

#########################################################
# Asign result of SP to CN_SI_PBC in layer wsk_urb_ter_uz
#########################################################

in_excel = r'E:\WODY_WAWA\SKRYPTY\Wynik_koncowy_SP_zlew_2019.xlsx'
arcpy.ExcelToTable_conversion(in_excel, "Tabela_wynik_koncowy_SP_zlew", "Arkusz2")
print(arcpy.GetCount_management("Tabela_wynik_koncowy_SP_zlew"))
# <Result '38960'>
arcpy.JoinField_management("zlewnie_warszawa_podzial_KW", "ZlewShort", "Tabela_wynik_koncowy_SP_zlew", "Obreb", "Cn")
# <Result 'zlewnie_warszawa_podzial_KW'>
arcpy.SelectLayerByAttribute_management("zlewnie_warszawa_podzial_KW", "NEW_SELECTION", """"Cn" is null""")
# <Result 'zlewnie_warszawa_podzial_KW'>
print(arcpy.GetCount_management("zlewnie_warszawa_podzial_KW"))
# <Result '2'>
# calculate if CN is null
# arcpy.CalculateField_management("zlewnie_warszawa_podzial_KW", "Cn", "-9999","PYTHON_9.3")
# <Result 'zlewnie_warszawa_podzial_KW'>
arcpy.SelectLayerByAttribute_management("zlewnie_warszawa_podzial_KW", "CLEAR_SELECTION")
arcpy.AddField_management("zlewnie_warszawa_podzial_KW", "SP", "DOUBLE", "", "", "")
arcpy.CalculateField_management("zlewnie_warszawa_podzial_KW", "SP", "!Cn!","PYTHON_9.3")
arcpy.DeleteField_management("zlewnie_warszawa_podzial_KW", "Cn")
# <Result 'zlewnie_warszawa_podzial_KW'>